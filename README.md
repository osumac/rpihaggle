# RPiHaggle
[![pipeline status](https://gitlab.com/osumac/rpihaggle/badges/master/pipeline.svg)](https://gitlab.com/osumac/rpihaggle/pipelines)

**Note**:  This repo currently contains some sample code to help us get familiar with Node, Express, React, Postgres, and Heroku.  This code can be removed and/or updated as the team sees fit.

### Team Links
| Webpage |
| ------- |
| [RPiHaggle](https://rpihaggle.herokuapp.com/) |
| [Heroku Dashboard](https://dashboard.heroku.com/apps/rpihaggle) |
| [AWS Console](https://123797903158.signin.aws.amazon.com/console) |
| [Google Drive](https://drive.google.com/drive/u/0/folders/1t2zArkYAVoqzppHx91LCKxFPGO9l3ciL) |
| [Course Website](https://sites.google.com/site/rpisdd/) |

### Installation
Be sure you have the following programs installed.
 - [Node.js](https://nodejs.org/)
 - [PostgreSQL](https://www.postgresql.org/download/)
 - [Heroku](https://devcenter.heroku.com/articles/heroku-cli#download-and-install) (optional)

Clone repo, install dependencies, and start the express server.

```sh
$ git clone https://gitlab.com/osumac/rpihaggle.git
$ git checkout -b <name>-dev #create your own development branch (ie. christine-dev)
$ npm install
$ npm start
```

Start the frontend client in a separate terminal.

```sh
$ cd frontend
$ npm install
$ npm start
```

Set the Heroku DATABASE_URL environment variable
```sh
$ export DATABASE_URL=postgres://errqqwivdgtxuo:bdf489e20b37d0a65aa9b9283aba08df36875ab0541d65f33582d53de9243ab0@ec2-54-163-246-159.compute-1.amazonaws.com:5432/ddiui2at46v1rs #Linux 
or
$ SET DATABASE_URL=postgres://errqqwivdgtxuo:bdf489e20b37d0a65aa9b9283aba08df36875ab0541d65f33582d53de9243ab0@ec2-54-163-246-159.compute-1.amazonaws.com:5432/ddiui2at46v1rs #Windows
```

Open that app at http://localhost:3000/.

You can also test Express API calls at http://localhost:5000/<request> (ie. http://localhost:5000/users)

### Create and Seed Local Dev Database
1. Copy the contents of `.env.example` to a file named `.env` in the same directory. Note: git will ignore your local `.env` file.
2. Using pgAdmin or the commandline, create a database name 'rpihaggle'

  The code assumes the username is 'postgres' and the password is 'root', but you can update these values by changing `knexfile.js` and your `.env` file.

3. Run the following commands in the project's root directory
```sh
knex migrate:rollback --env development     #removes existing database tables
knex migrate:latest --env development       #create the database tables
knex seed:run --env development             #seeds the database
```


### Production Deployment
```sh
heroku run knex migrate:rollback    #removes existing database tables
heroku run knex migrate:latest      #creates the database tables
heroku run knex seed:run            #seeds the database tables
```

### Pushing Your Local Changes
```sh
git status
git add -A .                                    #stages all files for commit
OR
git add <filename>                              #stages a specific file for commit
git commit                                      #commits your changes. you'll be prompted to type a commit message
OR
git commit -m "<your commit message here>"      #commits your changes with a commit message
git push -u origin <your branch name>           #push your local changes to the remote repository
```

### Pull and Merge the Staging Branch
```sh
git checkout staging
git pull
git checkout <your branch name>
git merge staging
```

### Helpful Tutorials
* [React Semantic UI](https://react.semantic-ui.com/)
* [Knex Schema Builder](https://knexjs.org/#Schema)
* [Donor App - Node + React + Express + MySQL](https://medium.com/@avanthikameenakshi/crud-react-express-99025f03f06e)
* [OOP Node](https://medium.com/@jennifernghinguyen_28211/node-js-ejs-template-express-and-object-oriented-programming-web-development-study-journal-1-1-f0415ffe9745)
* [Deploy Node + Postgres + Heroku](https://codeselfstudy.com/blog/deploy-node-postgres-heroku/)
* [Knex - Seed Postgres Database](https://medium.com/@jaeger.rob/seed-knex-postgresql-database-with-json-data-3677c6e7c9bc)