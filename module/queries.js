/*
 *
 * Queries contains of the database queries the application creates
 *
 */
const dotenv = require('dotenv');
dotenv.config();
const { Pool } = require('pg');
const { DATABASE_URL } = process.env;
const PG_SSL = process.env.PG_SSL ? false : true;
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');
const aws = require('../module/aws');
const multiparty = require('multiparty');
const fs = require('fs');
const fileType = require('file-type');

const pool = new Pool({
  connectionString: DATABASE_URL,
  ssl: PG_SSL
});


/*
*
* QUERIES
*
*/

// Purpose: get all users from the database
// Definition: 
const getUsers = (request, response) => {
  pool.query('SELECT "users".id, name, email, "screenName", "phoneNumber", address, "dateCreated", role FROM users INNER JOIN "userRoles" ON "users"."roleID" = "userRoles"."id" ORDER BY email ASC', [], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: get all reports from the database
// Definition: 
const getReports = (request, response) => {
  pool.query('SELECT * FROM reports', [], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}


// Purpose: get a single user from the database from their ID
// Definition: 
const getUserByID = (request, response) => {
  const id = request.params.id;
  pool.query('SELECT name, email, "screenName", "phoneNumber", address, filename, "dateCreated", "roleID" \
    FROM users \
    LEFT JOIN avatars on "avatarID" = avatars.id \
    WHERE users.id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows);
  });
}

// Purpose: get a user's favorite listings
// Definition: 
const getFavoritesByUserID = (request, response) => {
  const id = request.params.id;

  pool.query('SELECT DISTINCT ON(listings.id) * FROM favorites \
    JOIN listings ON favorites."listingID" = listings.id \
    LEFT JOIN "listingImages" ON favorites."listingID" = "listingImages"."listingID" \
    WHERE favorites."userID" = $1 AND "statusID" != 4', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: add a user favorite
// Definition: 
const addFavorite = (request, response) => {
  const { userID, listingID } = request.body;
  console.log(request.body);
  pool.query(
    'INSERT INTO favorites VALUES ($1, $2)',
    [userID, listingID],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send({message: `Favorite Added`});
    }
  )
}

// Purpose: remove a user favorite
// Definition: 
const removeFavorite = (request, response) => {
  const { userID, listingID } = request.body;
  console.log(request.body);
  pool.query(
    'DELETE FROM favorites WHERE "userID" = $1 and "listingID" = $2',
    [userID, listingID],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send({message: `Favorite Removed`});
    }
  )
}

// Purpose: get a user's posted listings
// Definition: 
const getListingsByUserID = (request, response) => {
  const id = request.params.id;

  pool.query('SELECT DISTINCT ON(listings.id) * FROM listings \
    LEFT JOIN "listingImages" ON listings.id = "listingImages"."listingID" \
    WHERE listings."userID" = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: get all listings from the database
// Definition: 
const getListings = (request, response) => {
  pool.query('SELECT DISTINCT ON(listings.id) listings.id, name, "userID", description, "dateCreated", "statusID", "conditionID", "isFree", "listingID", filename FROM listings \
    LEFT JOIN "listingImages" ON listings.id = "listingImages"."listingID" \
    LEFT JOIN "listingStatuses" on "listingStatuses".id = "statusID" \
    WHERE status = \'Active\'', [], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: search listings by name / description
// Definition: 
const searchListings = (request, response) => {
  const queryString = '%' +  request.body.queryString.toLowerCase() + '%';
  pool.query('SELECT DISTINCT ON(listings.id) listings.id, name, "userID", description, "dateCreated", "statusID", "conditionID", "isFree", "listingID", filename FROM listings \
    LEFT JOIN "listingImages" ON listings.id = "listingImages"."listingID" \
    LEFT JOIN "listingStatuses" on "listingStatuses".id = "statusID" \
    WHERE status = \'Active\' AND (lower(name) like $1 OR lower(description) like $1)', [queryString], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: get a user based on their email
// Definition: 
const getUserByEmail = (request, response) => {
  const email = request.params.email;

  pool.query('SELECT * FROM users WHERE email = $1', [email], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: get one individual listing
// Definition: 
const getListingByID = (request, response) => {
  const id = request.params.id;

  pool.query('SELECT listings.name, description, listings."dateCreated", "isFree", listings."userID", "screenName", status, "statusID", "conditionID", "condition" FROM listings \
    JOIN users on listings."userID" = users.id \
    JOIN "listingStatuses" on "listingStatuses".id = "statusID" \
    JOIN "listingConditions" on "listingConditions".id = "conditionID" \
    WHERE listings."id" = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: get the images corresponding to a listing
// Definition: 
const getListingImages = (request, response) => {
  const id = request.params.id;

  pool.query('SELECT filename FROM "listingImages" WHERE "listingID" = $1', [id], (error, results) => {
    if (error) {
      throw error
    } else {
      if(results.rowCount == 0){
        response.status(201).send({images: []});
      } else {
        response.status(201).send(results.rows);
      }
    }
  });
}

// Purpose: get the items a user is looking for
// Definition: 
const getLookingFor = (request, response) => {
  const id = request.params.id;
  pool.query('SELECT "lookingFor".id, description, "dateCreated", name FROM "lookingFor" \
    JOIN tags on "tagID" = tags.id \
    WHERE "userID" = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: get the item's a posted listing wants in return
// Definition: 
const getLookingForByListingID = (request, response) => {
  const id = request.params.id;
  pool.query('SELECT "lookingFor".id, description, "dateCreated", name FROM "lookingFor" \
    JOIN tags on "tagID" = tags.id \
    WHERE "userID" = (SELECT "userID" from listings WHERE id = $1)', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: get all the tags from the database
// Definition: 
const getTags = (request, response) => {
  pool.query('WITH RECURSIVE tag_tree (id, name, "parentTagID", level, "clasifierID") as (\
    SELECT id, name, "parentTagID", 0, id AS "clasifierID" \
    FROM tags WHERE "parentTagID" IS null \
    UNION ALL \
    SELECT c.id, c.name, c."parentTagID", p.level + 1, p."clasifierID" \
    FROM tags c \
      JOIN tag_tree p ON p.id = c."parentTagID" \
  ) \
  SELECT * FROM tag_tree \
  ORDER BY "clasifierID", "parentTagID" DESC', [], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  });
}

// Purpose: check if the user's credentials are correct, and send necessary information back
// Definition: 
const logIn = async (request, response) => {
  const form = new multiparty.Form();
  form.parse(request, function(error, fields, files) {
    if (error) throw new Error(error);
    let { email, password } = fields;
    pool.query('SELECT id, email, password, "screenName", string_agg("listingID"::text, \', \') as favorites FROM users  \
      LEFT JOIN favorites ON "userID" = id \
      WHERE email = $1 \
      GROUP BY id, email, password, "screenName" \
      LIMIT 1', [email[0]], (error, results) => {
      if (error) {
        throw error;
      }

      if (results.rows.length != 0) {
        let passhash = results.rows[0].password;
        //Validate the user password
        bcrypt.compare(password[0], passhash, function(err, result) {
          if(err){
            console.log('bcrypt error:', err);
          }
          if (result == true) {
            let user = results.rows[0];
            const body = {
              id: user.id,
              email: user.email,
              screenName: user.screenName,
              favorites: user.favorites
            };
            //Store user in session
            //request.session.user = body;

            //Sign the JWT token and populate the payload with the user email favorites and id
            const token = jwt.sign({
              user: body
            }, 'rpihaggle', { expiresIn: '1d' });

            // Send the Cookie header
            response.cookie('jwt', token, {
              signed: true,
            });
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1); 
            //Send back the token to the user
            response.send({
              loggedIn: true, 
              user: JSON.stringify({
                screenName: results.rows[0].screenName,
                name: results.rows[0].name,
                favorites: results.rows[0].favorites
              }), 
              jwt: token,
              expires: tomorrow,
              id: body.id
            });
          } else {
            response.send({loggedIn: false, message: 'Incorrect Username and Password'});
          }
        });
      }
      else {
        response.send({loggedIn: false, message: 'Incorrect Username and Password'});
      }
    })
  });
}


/* 
*
* CREATES
*
*/

// Purpose: create a user and save them to the database
// Definition: 
const createUser = (request, response) => {
  console.log('create user');
  const form = new multiparty.Form();
  form.parse(request, function(error, fields, files) {
    if (error) throw new Error(error);
    let {name, email, password, phoneNumber, screenName, address} = fields;
    name = name[0];
    email = email[0];
    password = password[0];
    phoneNumber = phoneNumber[0];
    screenName = screenName[0];
    address = address[0];
    console.log(name + " " + email + " "+ password + " "+ phoneNumber + " "+ screenName);
    pool.query('SELECT * FROM users WHERE email = $1 LIMIT 1', [email], (error, results) => {
      if (error) {
        throw error
      }
      
      console.log(results.rowCount);
      var passwordHash = "";
      if(results.rowCount == 0){
        pool.query('SELECT * FROM users WHERE "screenName" = $1 LIMIT 1', [screenName], (error, results) => {
          if (error) {
            throw error
          }
          
          console.log(results.rowCount);
          var passwordHash = "";
          if(results.rowCount == 0){
            bcrypt.hash(password, 10, function(err, hash) {
              if (err) {
                return next(err);
              }
              console.log('hashed')
              let date = new Date();
              
              pool.query('INSERT INTO users (name, email, password, "screenName", "phoneNumber", "dateCreated", address) VALUES ($1, $2, $3, $4, $5, $6, $7)', [name, email, hash, screenName, phoneNumber, date, address], (error, results) => {
                try {
                  if (error) {
                    throw error
                  }
                  response.status(201).send({created: true})
                }
                catch (err){
                  console.log(err)
                  response.status(500).send({created: false});
                }
              });
            })
          }
          else{
            response.send({created: false, message: "screen name already exists"});
          }
        });
      } else{
        response.send({created: false, message: "email already exists"});
      }
    });
  });
}


// Purpose: create a new listing in the application
// Definition: 
const createNewListing = function(request, response){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;

  var yyyy = today.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  } 
  if (mm < 10) {
    mm = '0' + mm;
  } 
  var today = mm + '/' + dd + '/' + yyyy;

  const form = new multiparty.Form();
  form.parse(request, async (error, fields, files) => {
    if (error) throw new Error(error);
    const { userid, name, categories, condition, isFree, description } = fields;
    await pool.query('INSERT INTO  listings (name, "userID", description, "dateCreated", "statusID", "conditionID", "isFree") VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *', [name[0], userid[0], description[0], today, 1, condition[0], isFree[0]], (error, results) => {
      if (error) {
        throw error;
      }
      else {
        const listingID = results.rows[0].id;
        if(Object.keys(files).length > 0){
          try {
            for(const file of files.file){
              const path = file.path;
              const buffer = fs.readFileSync(path);
              const type = fileType(buffer);       
              const timestamp = Date.now().toString();
              const fileName = `listingImages/${timestamp}-lg`;
              const data = aws.uploadFile(buffer, fileName, type);
              pool.query('INSERT INTO "listingImages" ("listingID", filename) VALUES ($1, $2)', [listingID, fileName+'.'+type.ext], (error, results) => {
                try {
                  if (error) throw error
                } catch (err){
                  console.log(err);
                  response.status(500).send({listingCreated: false, error: 'Error Uploading Listing Images'});
                }
              });
            }
            console.log('done');
            response.status(200).send({listingCreated: true, redirectPath: '/myListings'});
          } catch (error) {
            console.log(error);
            return response.status(400).send({listingCreated: false, error: 'Error Creating Listing'});
          }
        } else {
          console.log('done');
          response.status(200).send({listingCreated: true, redirectPath: '/myListings'});
        }
      }
    });
  });
}


/* 
*
* UPDATES
*
*/

// Purpose: update a user's information in the database
// Definition: 
const updateUser = (request, response) => {
  const { id, name, phone_number, screen_name, address } = request.body

  pool.query(
    'UPDATE users SET name = $1, "phoneNumber" = $2, "screenName" = $3, address = $4 WHERE id = $5',
    [name, phone_number, screen_name, address, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send({message: `User modified with id: ${id}`, redirectPath: '/profile'});
    }
  )
}


// Purpose: update a listing's information
// Definition: 
const updateListing = (request, response) => {
  const { id, statusID, conditionID, description} = request.body
  console.log(request.body)
  pool.query(
    'UPDATE listings SET "statusID" = $1, "conditionID" = $2, description = $3 WHERE id = $4',
    [statusID, conditionID, description, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send({message: `Listing modified with id: ${id}`, redirectPath: '/myListings'});
    }
  )
}

// Purpose: report a listing
// Definition: 
const reportListing = (request, response) => {
  const { userID, listingID, reason, description} = request.body
  console.log('body', request.body)
  pool.query(
    'INSERT into reports ("reporterID", reason, description, "listingID") VALUES($1, $2, $3, $4)',
    [userID, reason, description, listingID],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send({message: 'Report added', redirectPath: '/home'});
    }
  )
}


/* 
*
* DELETES
*
*/

// Purpose: delete a user from the database
// Definition: 
const deleteUser = (request, response) => {
  const email = parseInt(request.params.email);

  pool.query('DELETE FROM users WHERE email = $1', [email], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with EMAIL: ${email}`)
  })
}



module.exports = {
  getUsers,
  getUserByEmail,
  getUserByID,
  getReports,
  getFavoritesByUserID,
  getListingsByUserID,
  getListings,
  getListingByID,
  getLookingFor,
  getLookingForByListingID,
  getTags,
  getListingImages,
  searchListings,
  createNewListing,
  updateListing,
  reportListing,
  addFavorite,
  removeFavorite,
  createUser,
  updateUser,
  deleteUser,
  logIn
}