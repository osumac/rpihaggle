const AWS = require('aws-sdk');
const bluebird = require('bluebird');

// configure the keys for accessing AWS
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
// configure AWS to work with promises
AWS.config.setPromisesDependency(bluebird);
// create S3 instance
const s3 = new AWS.S3();
// abstracts function to upload a file returning a promise
const uploadFile = (buffer, name, type) => {
  const params = {
    ACL: 'public-read',
    Body: buffer,
    Bucket: process.env.S3_BUCKET,
    ContentType: type.mime,
    Key: `${name}.${type.ext}`
  };
  promise = s3.upload(params).promise();
  promise.then(function(data){
    console.log('upload succeeded');
  }).catch(function(err) {
    console.log(process.env.AWS_ACCESS_KEY_ID)
    console.log(process.env.AWS_SECRET_ACCESS_KEY)
    console.log(process.env.S3_BUCKET)
    console.log('s3 upload error:', err);
  });
};

const getImage = (key) => {
  const params = {
    Bucket: process.env.S3_BUCKET,
    Key: key
  };
  s3.getObject(params, function(err, data) {
    return data.Body;
    /* res.writeHead(200, {'Content-Type': 'image/jpeg'});
    res.write(data.Body, 'binary');
    res.end(null, 'binary'); */
  });
};

module.exports = {
  uploadFile,
  getImage
}