module.exports = [
  { "name": "Computers, Tablets & Network Hardware", "parentTagID": 1  },
  { "name": "Cameras", "parentTagID": 1 },
  { "name": "Cell Phones, Smart Watches, & Accessories", "parentTagID": 1 },
  { "name": "Shoes", "parentTagID": 2 },
  { "name": "Jewelry", "parentTagID": 2 },
  { "name": "Handbags & Accessories", "parentTagID": 2 },
  { "name": "Indoor Sports", "parentTagID": 3 },
  { "name": "Outdoor Sports", "parentTagID": 3 },
  { "name": "Antiques", "parentTagID": 4 },
  { "name": "Art", "parentTagID": 4 },
  { "name": "Comics", "parentTagID": 4 }
];