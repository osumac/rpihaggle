module.exports = [
  { "name": "Game Cube", "userID": 1, "description": "Cool gaming console.", "dateCreated": "2/1/2019", "statusID": 1, "conditionID": 1, "isFree":false, "dateUpdated": "" },
  { "name": "Game of Thrones DVD Collection", "userID": 1, "description": "Seasons 1-3", "dateCreated": "2/15/2019", "statusID": 1, "conditionID": 2, "isFree":false, "dateUpdated": "" },
  { "name": "Nike Shox", "userID": 2, "description": "Size 10 shoe. Lightly worn. Pink and black", "dateCreated": "2/1/2019", "statusID": 1, "conditionID": 3, "isFree":false, "dateUpdated": "" },
  { "name": "Futon", "userID": 2, "description": "Black Futon. Comfy Chair. 2 years old. Good condition.", "dateCreated": "2/2/2019", "statusID": 1, "conditionID": 4, "isFree":false, "dateUpdated": "2/24/2019" },
  { "name": "Chair", "userID": 2, "description": "Wooden desk chair.", "dateCreated": "2/10/2019", "statusID": 2, "conditionID": 5, "isFree":true, "dateUpdated": "" }
];