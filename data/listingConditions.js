module.exports = [
  { "condition": "New" },
  { "condition": "Like New" },
  { "condition": "Excellent" },
  { "condition": "Good" },
  { "condition": "Acceptable" }
];