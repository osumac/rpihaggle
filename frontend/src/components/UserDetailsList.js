import React, { Component } from 'react';
import { List } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

// Purpose: display all of a user's information
// State:
//      user: the user being displayed's data
//      userID: userID of user being loaded
//      isLoading: is the data still loading
// Definition: 
class UserDetailsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      userID: this.props.id,
      isLoading: true
    }
  }
  // Purpose: Load a user's information and store it in the state
  // Definition: 
  componentDidMount() {
    let self = this;
    fetch('/user/' + this.state.userID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ user: data[0], isLoading: false  });
    }).catch(err => {
      console.log('Error getting user data:');
      throw(err);
    })
  }

  render() {
    return (
      this.state.isLoading ? 
      <List>

      </List> :
      <List>
        <List.Item>Name: {this.state.user.name}</List.Item>
        <List.Item>Email: {this.state.user.email}</List.Item>
        <List.Item>Screen Name: {this.state.user.screenName}</List.Item>
        <List.Item>Phone Number: {this.state.user.phoneNumber}</List.Item>
        <List.Item>Address: {this.state.user.address}</List.Item>
        <List.Item>Member Since: {(new Date(this.state.user.dateCreated)).toDateString()}</List.Item>
      </List>
    );
  }
}

export default UserDetailsList;
