import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
// Purpose: show Listing's images
// Definition: 
class ImageCarousel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      listingID: this.props.id,
      images: []
    }
  }

  // Purpose: get images from the database
  // Definition: 
  componentDidMount() {
    let self = this;
    fetch('/listingImages/' + this.state.listingID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ images: data, isLoading: false  });
    }).catch(err => {
      console.log('Error getting images:');
      throw(err);
    });
  }

  render() {
    const IMG_URL= "https://s3.amazonaws.com/rpihaggle-images/";
    const indicators = this.state.images.length > 0 ? this.state.images.map((entry, index) => (
      <li key={index} data-target="#myCarousel" data-slide-to={index} className={index===0? "active": ""}></li>
    )) : (
      <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
    );
    const images = this.state.images.length > 0 ? this.state.images.map((entry, index) => (
      <div key={index} className={index===0? "item active": "item"}>
        <img src={IMG_URL+entry.filename}  alt={entry.filename}/>
      </div>
    )) : (
      <div className="item active">
        <img src="https://via.placeholder.com/250x200?text=No+Image"  alt="NONE"/>
      </div>
    );
    
    return (
      <div id="myCarousel" className="carousel slide" data-interval="false">
        <ol className="carousel-indicators">
          {indicators}
        </ol>
        <div className="carousel-inner">
          {images}
        </div>
        <a className="left carousel-control" href="#myCarousel" data-slide="prev">
          <span className="glyphicon glyphicon-chevron-left"></span>
          <span className="sr-only">Previous</span>
        </a>
        <a className="right carousel-control" href="#myCarousel" data-slide="next">
          <span className="glyphicon glyphicon-chevron-right"></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
    );
  }
}

export default ImageCarousel;
