import React, { Component } from 'react';
import { List } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

// Purpose: display the list of items a user is looking for
// Definition: 
class LookingForList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      userID: this.props.id,
      listingID: this.props.listingID,
      lookingFor: []
    }
  }

  // Purpose: get the items a user is looking for from the database
  // Definition: 
  componentDidMount() {
    let self = this;
    const url = this.state.userID ? '/lookingFor/' + this.state.userID : '/lookingForByListingID/' + this.state.listingID;
    fetch(url, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ lookingFor: data });
    }).catch(err => {
      console.log('Error getting user data:');
      throw(err);
    })
  }

  render() {
    const lookingForData = this.state.lookingFor.length > 0 ? this.state.lookingFor.map((entry, index) => (
      <List.Item key={entry.id}>
        <List.Icon name='bullseye' />
        <List.Content>
          <List.Header content={entry.name}></List.Header>
          <List.Description content={entry.description}></List.Description>
        </List.Content>
      </List.Item>
    )) : (
      <List.Item>
        <List.Header>None</List.Header>
      </List.Item>
    );
  
    return (
      <List>
        {lookingForData}
      </List>
    );
  }
}

export default LookingForList;
