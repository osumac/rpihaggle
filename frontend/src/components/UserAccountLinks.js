import React, { Component } from 'react';
import { List } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {Link} from 'react-router-dom';

// Purpose: display actions a user can take from their profile page
// Definition: 
class UserAccountLinks extends Component {
  render() {
    return (
      <List>
        <List.Item icon='shopping bag' content={<Link to='/myListings'>My Listings</Link>} />
        <List.Item icon='plus' content={<Link to='/newlisting'>Create New Listing</Link>} />
        <List.Item icon='chat' content={<Link to='/messaging'>My Messages</Link>} />
        <List.Item icon='handshake' content={<Link to='#'>My Trades</Link>} />
        <List.Item icon='legal' content={<Link to='#'>Resolution Center</Link>} />
      </List>
    );
  }
}

export default UserAccountLinks;
