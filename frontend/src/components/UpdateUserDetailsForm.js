import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
// Purpose: display all of a user's information
// State:
      // user: user's data
      // userID: the current user's id
      // isLoading: is the data still loading
      // name: new name for user
      // phone_number: new phone number for user
      // screen_name: new screen name for user
      // address: new address for user
// Definition: 
class UpdateUserDetailsForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      userID: this.props.id,
      isLoading: true,
      name: '',
      phone_number: '',
      screen_name: '',
      address: ''
    }
    this.logChange = this.logChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // Purpose: update the state when a user changes form fields
  // Definition: 
  logChange(e) {
    if (e.target) {
      this.setState({ [e.target.name]: e.target.value });
    }
  }

  // Purpose: save the new information that a user gives to the application
  // Definition:  
  handleSubmit(event) {
    event.preventDefault();
    var data = {
      id: this.state.userID,
      name: this.state.name,
      phone_number: this.state.phone_number,
      screen_name: this.state.screen_name,
      address: this.state.address
    }
    fetch('/updateUser', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      window.location.href = data.redirectPath;
    }).catch(err => {
      console.log('Error getting user data:', err);
      throw(err);
    })
  }

  // Purpose: load the users data so that it can be changed
  // Definition: 
  componentDidMount() {
    let self = this;
    fetch('/user/' + this.state.userID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ user: data[0], isLoading: false, name: data[0].name, phone_number: data[0].phone_number, screen_name: data[0].screen_name, address: data[0].address  });
    }).catch(err => {
      console.log('Error getting user data:');
      throw(err);
    })
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit} method="POST">
        <Form.Field>
          <label>Name</label>
          <input onChange={this.logChange} name='name' defaultValue={this.state.user.name} className="form-control"  />
        </Form.Field>
        <Form.Field>
          <label>Screen Name</label>
          <input onChange={this.logChange} name='screen_name' defaultValue={this.state.user.screenName} className="form-control"  />
        </Form.Field>
        <Form.Field>
          <label>Phone Number</label>
          <input onChange={this.logChange} name='phone_number' defaultValue={this.state.user.phoneNumber} className="form-control"  />
        </Form.Field>
        <Form.Field>
          <label>Address</label>
          <input onChange={this.logChange} name='address' defaultValue={this.state.user.address} className="form-control"  />
        </Form.Field>
        <Button type='submit'>Submit</Button>
      </Form>
    );
  }
}

export default UpdateUserDetailsForm;
