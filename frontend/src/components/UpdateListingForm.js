import React, { Component } from 'react';
import { Form, Button, Dropdown } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

// Purpose: update a current listing
// State:
      // user: user's data
      // listingID: the current listing's id
      // listing: current listing data 
      // isLoading: is the listing still loading
      // statusID: what is the listings new status
      // conditionID: the condition of the listing item
      // description: new description of the listing
// Definition: 
class UpdateListingForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      listingID: this.props.id,
      listing: [],
      isLoading: true,
      statusID: 0,
      conditionID: 0,
      description: ''
    }
    this.logChange = this.logChange.bind(this);
    this.handleStatusChange = this.handleStatusChange.bind(this);
    this.handleConditionChange = this.handleConditionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // Purpose: update the state when a user changes form fields
  // Definition: 
  logChange(e) {
    if (e.target) {
      this.setState({ [e.target.name]: e.target.value });
    }
  }

  // Purpose: update the state when a user changes the status
  // Definition: 
  handleStatusChange(e, data){
    this.setState({ statusID: data.value });
  }

   // Purpose: update the state when a user changes the condition
  // Definition: 
  handleConditionChange(e, data){
    this.setState({ conditionID: data.value });
  }


  // Purpose: save the new listing information to the database
  // Definition: 
  handleSubmit(event) {
    event.preventDefault();
    var data = {
      id: this.state.listingID,
      statusID: this.state.statusID,
      conditionID: this.state.conditionID,
      description: this.state.description
    }
    fetch('/updateListing', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      window.location.href = data.redirectPath;
    }).catch(err => {
      console.log('Error getting user data:', err);
      throw(err);
    })
  }

  // Purpose: get the listing being updated
  // Definition: 
  componentWillMount() {
    let self = this;
    fetch('/listing/' + this.state.listingID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      console.log(data);
      self.setState({ listing: data[0], isLoading: false, statusID: data[0].statusID, conditionID: data[0].conditionID, description: data[0].description  });
    }).catch(err => {
      console.log('Error getting listing data:');
      throw(err);
    })
  }

  render() {
    const statuses = [{id: 1, name: "Active"}, {id: 2, name: "Traded"}, {id: 3, name: "Expired"}, {id: 4, name: "Deleted"}]
    const conditions = [{id: 1, name: "New"}, {id: 2, name: "Like New"}, {id: 3, name: "Excellent"}, {id: 4, name: "Good"}, {id: 5, name: "Acceptable"}]
    
    return (
      <Form onSubmit={this.handleSubmit} method="POST">
        <Form.Field>
          <label>Status</label>
          <Dropdown className="form-control" fluid selection value={this.state.statusID} onChange={this.handleStatusChange} name='statusID'
                  options={statuses.map(status => ({
                        key: status.id,
                        value: status.id,
                        text: status.name,
                  }))}/>
        </Form.Field>
        <Form.Field>
          <label>Condition</label>
          <Dropdown className="form-control" fluid selection value={this.state.conditionID} onChange={this.handleConditionChange} name='conditionID'
                  options={conditions.map(condition => ({
                        key: condition.id,
                        value: condition.id,
                        text: condition.name,
                  }))}/>
        </Form.Field>
        <Form.Field>
          <label>Description</label>
          <input onChange={this.logChange} name='description' defaultValue={this.state.listing.description} className="form-control"  />
        </Form.Field>
        <Button type='submit'>Submit</Button>
      </Form>
    );
  }
}

export default UpdateListingForm;
