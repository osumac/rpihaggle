import React, { Component } from 'react';
import { Grid, Form, Button, Dropdown, Checkbox, Popup, Icon, Message} from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

// Purpose: create a new listing
// State:
      // user: user's data
      // tags: tags possible to be put under
      // name: name of the listing
      // categories: which tags the listing falls under
      // isFree: is the item being given away for free
      // images: images of the product
      // message: message from the server to show
      // description: product description
      // visible: is the message from the server visible
// Definition: 
class NewListingForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      tags: [],
      name: '',
      categories: [],
      condition: 0,
      isFree: false,
      images: null,
      message: '',
      visible: false,
      id: localStorage.getItem('id')

    }
    this.logChange = this.logChange.bind(this)
    this.handleCategoryChange = this.handleCategoryChange.bind(this)
    this.handleConditionChange = this.handleConditionChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  
  // Purpose: create the listing in the database and then display whether the listing was created or not
  // Definition: 
  handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData();
    if(this.state.images){
      for(const file of this.state.images) {
        formData.append('file', file);
      }
    }
    formData.append('userid', this.state.id);
    formData.append('name', this.state.name);
    formData.append('categories', this.state.categories);
    formData.append('condition', this.state.condition);
    formData.append('isFree', this.state.isFree);
    formData.append('description', this.state.description);

    console.log(formData);
    let self = this;
    fetch("/createNewListing", {
      method: "POST",
      body: formData
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      console.log(data);
      self.setState({ message: 'New Listing Created!', visible: true });
      setTimeout(function(){ window.location.href = data.redirectPath }, 2000);
    }).catch(err => {
      console.log('Error creating new listing:');
      throw(err);
    });
  }


  // Purpose: change the state based on form entries
  // Definition: 
  logChange(e) {
    if (e.target) {
      if (e.target.name === 'images') {
        this.setState({ [e.target.name]: e.target.files });
      }
      else {
        this.setState({ [e.target.name]: e.target.value });
      }
    }
  }

  // Purpose: change the selected categories
  // Definition: 
  handleCategoryChange(e, data){
    this.setState({ categories: data.value });
  }

  // Purpose: change the selected condition
  // Definition: 
  handleConditionChange(e, data){
    this.setState({ condition: data.value });
  }

  // Purpose: get tags from the database
  // Definition: 
  componentDidMount() {
    let self = this;
    fetch('/tags', {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      
      self.setState({ tags: data, isLoading: false  });
    }).catch(err => {
      console.log('Error getting tags:');
      throw(err);
    })
  }

  render() {
    const conditions = [{id: 1, name: "New"}, {id: 2, name: "Like New"}, {id: 3, name: "Excellent"}, {id: 4, name: "Good"}, {id: 5, name: "Acceptable"}]
    
    return (
      <Grid>
        <Grid.Column width={16}>
          <Form onSubmit={this.handleSubmit} method="POST">
            <Form.Field>
              <label>Item Name</label>
              <input onChange={this.logChange} className="form-control"  name='name' />
            </Form.Field>
            <Form.Field>
              <label>Item Categories</label>
              <Dropdown className="form-control" fluid multiple clearable search selection onChange={this.handleCategoryChange} name='categories'
                  options={this.state.tags.map(tag => ({
                        key: tag.id,
                        value: tag.id,
                        text: "\u00a0".repeat(tag.level*4) + tag.name,
                  }))}/>
            </Form.Field>
            <Form.Field>
              <label>Item Condition</label>
              <Dropdown className="form-control" fluid selection onChange={this.handleConditionChange} name='condition'
                  options={conditions.map(condition => ({
                        key: condition.id,
                        value: condition.id,
                        text: condition.name,
                  }))}/>
            </Form.Field>
            <Form.Field>
              <Checkbox label=' Free?' name='isFree' /> &nbsp;&nbsp;&nbsp;
              <Popup trigger={<Icon name='help circle' />} content='Check if you wish to give this item away for free.' />
            </Form.Field>
            <Form.Field>
              <label>Item Description</label>
              <textarea onChange={this.logChange} className="form-control" type='select'  name='description'></textarea>
            </Form.Field>
            <Form.Field>
              <label>Images</label>
              <input onChange={this.logChange} className="form-control" type='file' multiple name='images' />
            </Form.Field>
            <Message
              header={this.state.message}
              color='black'
              size='massive'
              hidden={!this.state.visible}
            />
            <Grid.Row className="submit-section">
              <Button type='submit' size='massive' color='grey'>Submit</Button>
            </Grid.Row>           
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

export default NewListingForm;
