import React, { Component } from 'react';
import { List, Image } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
//import {Link} from 'react-router-dom';

// Purpose: get thefavorite listings of a user
// Definition: 
class FavoritesList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      favorites: [],
      userID: this.props.id,
      isLoading: true
    }
  }
  // Purpose: get the favorited listings from the database
  // Definition: 
  componentDidMount() {
    let self = this;
    fetch('/favorites/' + this.state.userID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ favorites: data, isLoading: false  });
    }).catch(err => {
      console.log('Error getting favorites:');
      throw(err);
    })
  }

  render() {
    const IMG_URL= "https://s3.amazonaws.com/rpihaggle-images/";
    const listData = this.state.favorites.length > 0 ? this.state.favorites.map(entry => (
      <List.Item key={entry.id} href={'/listing/' + entry.id} >
        <Image src={(entry.filename != null) ? IMG_URL+entry.filename: "https://via.placeholder.com/100"} width={100} />
        <List.Content>
          <List.Header content={entry.name}></List.Header>
          <List.Description content={entry.description}></List.Description>
        </List.Content>
      </List.Item>
    )) : (
      <List.Item>
        <List.Header>None</List.Header>
      </List.Item>
    );
  
    return (
      <List className="favorite-listings">
        {listData}
      </List>
    );
  }
}

export default FavoritesList;
