import React, { Component } from 'react';
import { Grid, Container, List } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {Link} from 'react-router-dom';

export default class FooterComponent extends Component {
	render() {
		return (
				<footer className="site-footer">
					<Container>
						<Grid>
							<Grid.Row centered>
								<List horizontal relaxed='very' size='huge'>
									<List.Item><Link to="#">About RPiHaggle</Link></List.Item>
									<List.Item><Link to="#">Announcements</Link></List.Item>
									<List.Item><Link to="/admin">Admin</Link></List.Item>
									<List.Item><Link to="#">Policies</Link></List.Item>
									<List.Item><Link to="#">Help & Contact</Link></List.Item>
									<List.Item><Link to="#">Site Map</Link></List.Item>
								</List>
							</Grid.Row>
							<Grid.Row centered className="small-text">
								All rights reserved © 2019
							</Grid.Row>
						</Grid>
					</Container>
				</footer>
		);
	}
}