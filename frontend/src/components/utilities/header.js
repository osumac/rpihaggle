import React, { Component } from 'react';
import logo from '../../Logo.png';
import { Icon, Form, Input } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { logout as doLogout } from '../../util/util';
import { withRouter, Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import {isLoggedIn} from '../../util/util';

export default withRouter(class HeaderComponent extends Component {
  // static propTypes = {}
  // static defaultProps = {}
  // state = { user = [] }
  constructor(props) {
    super(props);
    this.state = {
      user: [],
      modalIsOpen: false,
      queryString: '',
      listings: [],
      search: false
    };
    this.loadLoggedInUser = this.loadLoggedInUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.logout  = this.logout.bind(this);
  }

  loadLoggedInUser() {
    fetch('/loginuser', {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      if (data === "no user") {
        this.setState({ user: "" })
      } else {
      }
    }).catch(err => {
      console.log('error caught!', err);
    })
  }

  logout() {
    doLogout();
    this.props.history.push({
      pathname: '/'});
  }

  handleChange(e){
    if (e.target) {
      this.setState({ [e.target.name]: e.target.value });
    }
  }

  onFormSubmit(e) {
    e.preventDefault();
    let self = this;
    var data = {
      queryString: self.state.queryString
    }
    fetch('/searchListings', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      localStorage.setItem('searchListings', JSON.stringify(data));
      window.location.href  = '/marketplace';
      /* self.setState({ search: true, listings: data });
      console.log(self.state); */
    }).catch(err => {
      console.log('error caught!', err);
    })
  }

  componentDidMount() {
    this.loadLoggedInUser.bind(this);  
  }

  render() {
    let loggedIn = isLoggedIn();
    var style = {
      backgroundImage: "url(" + logo + ")",
      backgroundSize: "contain",
      width: "100%",
      height: "100%",
      backgroundPosition: "center",
      display: "inline-block",
      backgroundRepeat: "no-repeat"
    }

    if(this.state.search) {
      return <Redirect to='/marketplace'/>
    }

    return (
      <div className="app-header">

        <Link to="/" className="logo-a"><span style={style}></span></Link>
        
        <div className="header-search">
          <Form onSubmit={this.onFormSubmit}>
            <Input size='massive' icon placeholder='Search...'>
              <input className="form-control" onChange={this.handleChange} name="queryString" />
              <Icon color='grey' name='search' />
            </Input>
          </Form>
        </div>
        
        <div className="header-end">
          {
            loggedIn ?
            // eslint-disable-next-line
            <a className="header-icon" onClick={this.logout} >
              <Icon name='user circle' size='big' /> 
              <span>Logout</span>
            </a>: null
          }
        </div>

      </div>
    );
  }
})