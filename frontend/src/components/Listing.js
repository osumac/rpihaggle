import React, { Component } from 'react';
import { List, Grid, Header, Icon, Button, Popup } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import ImageCarousel from '../components/ImageCarousel';
import UpdateListingForm from '../components/UpdateListingForm';
import ReportListingForm from '../components/ReportListingForm';
import {Link} from 'react-router-dom';

// Purpose: display the listing information
// State:
      // user: user's data
      // userID: current user's ID
      // listingID: current listing's ID
      // listing: information about the listing
      // myListings: all of the current user's listings
      // isMyListing: is the current listing created by the current user
      // isFavorite: is the current listing in the user's favorites
      // showUpdateForm: show the edit listing form or not
// Definition: 
class Listing extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      userID: this.props.userID,
      listingID: this.props.listingID,
      listing: [],
      listing: [],
      myListings: [],
      isMyListing: false,
      isFavorite: false,
      showUpdateForm: false,
      showReportForm: false
    }
    this.saveFavorite = this.saveFavorite.bind(this);
    this.showEditForm = this.showEditForm.bind(this);
    this.reportListing = this.reportListing.bind(this);
  }

  // Purpose: will save a listing as a user's favorite
  // Definition: 
  saveFavorite(event) {
    //event.preventDefault();
    let self = this;
    let url = this.state.isFavorite ? '/removeFavorite' : '/addFavorite'
    var data = {
      userID: this.state.userID,
      listingID: this.state.listingID
    }
    
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ isFavorite: !self.state.isFavorite });
      let { favorites } = JSON.parse(localStorage.user);
      if (favorites != null){
        favorites = favorites.split(',').map(item => item.trim());
        if (!self.state.isFavorite && favorites.includes(self.state.listingID)) {
          var index = favorites.indexOf(self.state.listingID);
          favorites.splice(index, 1);
        } else if (self.state.isFavorite) {
          favorites.push(self.state.listingID);
        }
      } else if (self.state.isFavorite) {
        favorites.push(self.state.listingID);
      }
      let user = JSON.parse(localStorage.user);
      user.favorites = favorites.toString();
      localStorage.setItem('user', JSON.stringify(user));
      
    }).catch(err => {
      console.log('Error saving favorites:');
      throw(err);
    });
  }

  // Purpose: Report inappropriate / spam lising
  // Definition: 
  reportListing(){
    console.log('reported');
    console.log(this.state.listingID)
    console.log(this.state.userID)
    this.setState({ showReportForm: !this.state.showReportForm });
  }

  // Purpose: show/hide the form to update user profile settings
  // Definition: 
  showEditForm(){
    this.setState({ showUpdateForm: !this.state.showUpdateForm });
  }

  // Purpose: get the information about the listing so it can be displayed
  // Definition: 
  componentDidMount() {
    let self = this;
    let { favorites } = JSON.parse(localStorage.user);
    if (favorites != null){
      favorites = favorites.split(',').map(item => item.trim());
      if (favorites.includes(this.state.listingID)) {
        self.setState({ isFavorite: true });
      }
    }
    fetch('/myListings/' + this.state.userID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      // eslint-disable-next-line 
      var isMyListing = data.some((data) => { return (data.id == self.state.listingID) });
      self.setState({ isMyListing: isMyListing });
    }).catch(err => {
      console.log('Error getting user listings:');
      throw(err);
    });

    fetch('/listing/' + this.state.listingID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ listing: data[0], isLoading: false  });
    }).catch(err => {
      console.log('Error getting favorites:');
      throw(err);
    })
  }

  render() {
    return (
      <Grid>
        <Grid.Column width={16} float='right'>
          <Header>
            {this.state.listing.name}
            <Icon name={this.state.isMyListing ? 'pencil' : this.state.isFavorite ? 'star' : 'star outline'} color={this.state.isMyListing ? 'grey' : 'yellow'}  link onClick={this.state.isMyListing ? this.showEditForm : this.saveFavorite} style={{float:"right", fontSize:"20px"}} />
            {!this.state.isMyListing ?  <Popup trigger={  <Icon name='flag outline' color='grey' onClick={this.reportListing} style={{float:"right", fontSize:"20px"}} />}>
              Report this listing.
    </Popup>  : ''}
          </Header>
        </Grid.Column>
        <Grid.Column width={6}>
          <ImageCarousel id={this.state.listingID}/>
        </Grid.Column>
        <Grid.Column width={10}>
          <div hidden={this.state.showUpdateForm || this.state.showReportForm}>
            <List>
              <List.Item>Date Created: {this.state.listing.dateCreated}</List.Item>
              <List.Item>Status: {this.state.listing.status}</List.Item>
              <List.Item>Condition: {this.state.listing.condition}</List.Item>
              <List.Item>Description: {this.state.listing.description}</List.Item>
              {this.state.listing.screenName && <List.Item>User: <Link to={'/user/' + this.state.listing.userID}>{this.state.listing.screenName}</Link></List.Item>}
            </List>
          </div>
          <div hidden={!this.state.showUpdateForm}>
            <UpdateListingForm id={this.state.listingID}/>
          </div>
          <div hidden={!this.state.showReportForm}>
            <ReportListingForm userID={this.state.userID} listingID={this.state.listingID}/>
          </div>
        </Grid.Column>
      </Grid>
    );
  }
}

export default Listing;
