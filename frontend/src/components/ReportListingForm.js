import React, { Component } from 'react';
import { Form, Button, Dropdown } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

// Purpose: report a user's listing
// State:
      // user: user's data
      // listingID: the current listing's id
      // reason: reason for reporting the listing
      // description: details about the reason for reporting the listing
// Definition: 
class ReportListingForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userID: this.props.userID,
      listingID: this.props.listingID,
      reason: '',
      description: ''
    }
    this.logChange = this.logChange.bind(this);
    this.handleReasonChange = this.handleReasonChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // Purpose: update the state when a user changes form fields
  // Definition: 
  logChange(e) {
    if (e.target) {
      this.setState({ [e.target.name]: e.target.value });
    }
  }

  // Purpose: update the state when a user changes report reason
  // Definition: 
  handleReasonChange(e, data){
    this.setState({ reason: data.value });
  }

  // Purpose: save the report information to the database
  // Definition: 
  handleSubmit(event) {
    event.preventDefault();
    var data = {
      userID: this.state.userID,
      listingID: this.state.listingID,
      reason: this.state.reason,
      description: this.state.description
    }
    fetch('/reportListing', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      window.location.href = data.redirectPath;
    }).catch(err => {
      console.log('Error reporting listing:', err);
      throw(err);
    })
  }

  // Purpose: get the listing being reported
  // Definition: 
  componentWillMount() {
    let self = this;
  }

  render() {
    const reasons = [{id: 1, text: "Inappropriate"}, {id: 2, text: "Spam"}, {id: 3, text: "Miscategorized"}];
    
    return (
      <Form onSubmit={this.handleSubmit} method="POST">
        <Form.Field>
          <label>Reason</label>
          <Dropdown className="form-control" fluid selection onChange={this.handleReasonChange} name='reason'
                  options={reasons.map(reason => ({
                        key: reason.id,
                        value: reason.text,
                        text: reason.text,
                  }))}/>
        </Form.Field>
        <Form.Field>
          <label>Description</label>
          <input onChange={this.logChange} name='description' className="form-control"  />
        </Form.Field>
        <Button type='submit'>Submit</Button>
      </Form>
    );
  }
}

export default ReportListingForm;
