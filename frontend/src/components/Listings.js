import React, { Component } from 'react';
import { List, Image } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {Link} from 'react-router-dom';

// Purpose: display either a user's listings or all listings in the database
// Definition: 
class MyListings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      listings: this.props.listings && this.props.listings.length > 0 ? JSON.parse(this.props.listings) : [],
      userID: this.props.id,
      isLoading: true
    }
  }
  // Purpose: get the listings from the database
  // Definition: 
  componentWillMount() {
    if(this.state.listings.length === 0 && localStorage.getItem('searchListings') === null) {
      let self = this;
      const url = this.state.userID ? '/myListings/' + this.state.userID : '/allListings';
      fetch(url, {
        method: 'GET'
      }).then(function (response) {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      }).then(function (data) {
        self.setState({ listings: data, isLoading: false  });
      }).catch(err => {
        console.log('Error getting user listings:');
        throw(err);
      })
    } else {
      localStorage.removeItem('searchListings');
    }
  }

  render() {
    const IMG_URL= "https://s3.amazonaws.com/rpihaggle-images/";
    const listData = this.state.listings.length > 0 ? this.state.listings.map((entry, index) => (
      <div className="col-md-6" key={entry.id}>
        <Link to={'/listing/'+entry.id}>
          <div className="thumbnail">
            <Image src={(entry.filename != null) ? IMG_URL + entry.filename : "https://via.placeholder.com/242x200"} style={{height:"200px"}} alt="..." />
            <div className="caption">
              <h3>{entry.name}</h3>
              <p>{entry.description}</p>
            </div>
          </div>
        </Link>
      </div>
    )) : (
      <List.Item>
        <List.Header>No Listings Found</List.Header>
      </List.Item>
    );
  
    return (
      <List className="favorite-listings">
        {listData}
      </List>
    );
  }
}

export default MyListings;
