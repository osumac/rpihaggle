// Purpose: Check if a user is currently logged in to the applicatation
// Example: if a user is logged in, local storage will have a expires time for their JWT token. if this expires time is later than
//          the current time then the user is still logged in. If they have an expires time but it has passed, they get logged out
// Definition:
function isLoggedIn() {
    let expires = localStorage.getItem('expires');
    if (expires){
        console.log('is possibly logged in')
        let exp = new Date(expires);
        if (exp < Date.now()){
            logout();
            
            return false;
        }
        return true;
    }else
        return false;
}

// Purpose: logout a user from the application
// Example: user hits the logout button, their jwt and expires time get removed from storage
// Definition:
function logout(){
    console.log('doing logout');
    localStorage.removeItem('jwt');
    localStorage.removeItem('expires');
}
export {isLoggedIn, logout} ;