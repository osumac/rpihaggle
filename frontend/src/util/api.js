const apiUrl = 'http://localhost:5000'

// Purpose: Register a user for the application
// Example: user calls register. if the screen name and email are unique, the user is created
// Definition:
function doRegistration(user) {
    let body = JSON.stringify({user : user})
    console.log(body);

    const formData = new FormData();
    formData.append('name', user.name);
    formData.append('email', user.email);
    formData.append('phoneNumber', user.phoneNumber);
    formData.append('address', user.address);
    formData.append('screenName', user.screenName);
    formData.append('password', user.password);
    
    return fetch(apiUrl + "/register", {
      method: "POST",
      body: formData
    }).then((response) => {
      return response.json();
    }).catch((error) => {
      console.log("error:", error)
    })
  }

// Purpose: log in a user to the application
// Example: a user sends their email and password. the function will return their jwt, id, and expires if the login is correct. if not it returns 
//        a false in the json response
// Definition:
function doLogin (user){
  const formData = new FormData();
  formData.append('email', user.email);
  formData.append('password', user.password);

  return fetch(apiUrl + "/login", {
    method: "POST",
    body: formData
  }).then((response) => {
    return response.json();
  }).catch((error) => {
    console.log("error:", error)
  })
}

function test(){
  return fetch(apiUrl + '/test',
  {
      method: "POST"
  }).then((response) =>{
      console.log(response);
  }).catch((error) => {
      console.log("error:", error)
  })
}
  export {
      doRegistration,
      test,
      doLogin
  }