import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
import 'jquery/src/jquery.js';
import registerServiceWorker from './registerServiceWorker';
import HeaderComponent from './components/utilities/header';
import FooterComponent from './components/utilities/footer';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '@fortawesome/fontawesome-free';

// Purpose: load the react app in the correct format with the header, footer, and loaded route
// Definition:
ReactDOM.render(
	<BrowserRouter>
		<div className="appContent">
			<HeaderComponent />
			<Routes/>
			<FooterComponent />
		</div>
	</BrowserRouter>,
	document.getElementById('root'));

registerServiceWorker();