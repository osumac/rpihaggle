import React, { Component } from 'react';
import { Message } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import 'react-intl-tel-input/dist/main.css';
import 'react-select/dist/react-select.css';
import { doRegistration } from '../util/api'
import { withRouter } from 'react-router-dom';

// Purpose: Allows a user to register for the application
// State: 
    // name: user's name
    // email: user's email
    // phone_number: user's phone number
    // screenName: what hte user wishes to be seen as on the application
    // password: user's password
    // passwordConfirm: checks to make sure password is Correct
    // address: user's address
    // error: if there is an error registering
    // policy: if user has accepted the applicaitons policies
// Definition:
export default withRouter(class Registration extends Component {
  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'dark',
    time: 5000,
    transition: 'scale'
  }

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      phone_number: '',
      screenName: '',
      password: '',
      passwordConfirm: '',
      address: '',
      error: false,
      errorMessage: [],
      policy: 0,
    }
    this.logChange = this.logChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

  }
  // Purpose: performs validation on input fields and then calls the register function. 
  // Example: a user inputs all of his data and it is all validated. The function calls register, and when it succeeds it loads the login page
  // Definition:
  handleSubmit(event) {
    event.preventDefault()
    let errorMessage = [];
    if (this.state.password !== this.state.passwordConfirm) {
      errorMessage.push('Passwords do not match');
    }
    if (this.state.password === '') {
      errorMessage.push('Password is required')
    }
    var idx = this.state.email.indexOf('@rpi.edu');
    if (idx === -1) {
      errorMessage.push('RPI email address is required');
    }

    if(errorMessage.length > 0){
      this.setState({ error: true, errorMessage: errorMessage });
      return false;
    } else {
      this.setState({ error: false, errorMessage: errorMessage });
    }

    var data = {
      name: this.state.name,
      email: this.state.email,
      phoneNumber: this.state.phone_number,
      screenName: this.state.screenName,
      password: this.state.password,
      address: this.state.address
    }
    
    doRegistration(data).then((response, err) => {
      if (err)
        throw err;

      if (response.created) {
        this.props.history.push({
          pathname: '/login'
        });
      }
    });

  }

  // Purpose: manage the state when a user changes the data in any of the form fields
  // Example: user changes the email from abc@ to abc@r, the email's state is then changed to abc@r
  // Definition:
  logChange(e) {
    if (e.target) {
      if (e.target.name) {
        this.setState({ [e.target.name]: e.target.value });
      }
    }
  }
  // Purpose: loads all information to the screen that is required for the register page
  // Definition:
  render() {
    return (
      <div className="container register-form">
        <div className="heading-section">
          <div className="main-heading">
            Signup
          </div>
        </div>
        <div className="row">
          <div className="col-md-offset-2 col-md-8">
            <form onSubmit={this.handleSubmit} method="POST">
              <div className="panel panel-default p50 uth-panel">
              <div className="panel-body uth-panel-body">
                  <Message
                    list={this.state.errorMessage}
                    color='red'
                    compact
                    hidden={!this.state.error}
                  />
                  <div className="col-md-12">
                    <div className="form-wrap">
                      <label>Name</label>
                      <input onChange={this.logChange} className="form-control" value={this.state.name} placeholder='John Smith' name='name' validations={['required']} />
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-wrap">
                      <label>RPI Email</label>
                      <input onChange={this.logChange} className="form-control" value={this.state.email} placeholder='email@rpi.edu' name='email' validations={['required', 'email']} />
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-wrap">
                      <label>Phone Number</label>
                      <input onChange={this.logChange} className="form-control" value={this.state.phone_number} placeholder='555-555-555' type="tel" name="phone_number" />
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-wrap">
                      <label>Address</label>
                      <input onChange={this.logChange} className="form-control" value={this.state.address} placeholder='' type="tel" name="address" />
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-wrap">
                      <label>Screen Name</label>
                      <input onChange={this.logChange} className="form-control" value={this.state.screenName} name='screenName' validations={['required']} />
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-wrap">
                      <label>Password</label>
                      <input onChange={this.logChange} className="form-control" type='password' value={this.state.password} name='password' validations={['required', 'password']} />

                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-wrap">
                      <label>Password Confirm</label>
                      <input onChange={this.logChange} className="form-control" type='password' value={this.state.passwordConfirm} name='passwordConfirm' validations={['required', 'password']} />
                    </div>
                  </div>
                  <div className="row col-md-12 tc">
                    <input onChange={this.logChange} id='policy' type='checkbox' errorclassname='is-invalid-input' name='policy' value='1' validations={['required']} />
                    &nbsp;&nbsp;<label htmlFor="policy">I authorise the website to display my e-mail address. I also agree to the <a href="#/">terms and conditions.</a>
                    </label>
                  </div>
                  <div className="submit-section">
                    <button className="btn btn-uth-submit">Submit</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
})
