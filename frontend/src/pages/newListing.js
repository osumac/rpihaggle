import React, { Component } from 'react';
import { Container, Header, Grid, Segment } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import FavoritesList from '../components/FavoritesList';
import UserAccountLinks from '../components/UserAccountLinks';
import NewListingForm from '../components/NewListingForm';
import {isLoggedIn} from '../util/util'
import {withRouter} from 'react-router-dom';

class NewListing extends Component {
  constructor(props) {
    super(props);
    const splitUrl = this.props.location.pathname.split('/');
    this.state = { 
      listingID: splitUrl[splitUrl.length - 1],
      userID: localStorage.getItem('id')
    };
  }

  componentDidMount(){
    /* if(!isLoggedIn()){
      this.props.history.push({
        pathname: '/Login'});
    } */
  }

  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row>
            <Grid.Column width={6}>
              <Segment inverted className="panel-black">
                <Header size="medium">My Account</Header>
                <Grid>
                  <UserAccountLinks/>
                </Grid>
              </Segment>
              <Segment inverted className="panel-black">
                <Header size="medium">Favorites</Header>
                <Grid>
                  <FavoritesList id={this.state.userID}/>
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column width={10}>
              <Segment inverted className="panel-black">
              <Header>Create a New Listing</Header>
                <NewListingForm/>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}


export default  withRouter(NewListing);
