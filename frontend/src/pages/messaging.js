import React, { Component } from 'react';
import { List, Image, Container, Header, Segment, Grid } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

class Messaging extends Component {
  constructor(props){

    super(props);

    this.state={
      newMessage: '',
      messages: [
        '11:29 PM Christian Phillips: Hello I would like to buy your chair!',
        <br></br>,
        '11:45 PM You: Please let me know your avalibility',
        <br></br>,
        '11:49 PM Christian Phillips: I can come and see it at 12:45? Does that work for you?',
        <br></br>
      ]
    }
    this.handleChange = this.handleChange.bind(this);
    this.send = this.send.bind(this);
  }

  handleChange(e, data) {
    this.setState({ newMessage: e.target.value });
  }

  send(e){
    let newMessages = this.state.messages;
    var time = new Date();
    time = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) 
    const message  = time + " You: " + this.state.newMessage;
    newMessages.push(message);
    newMessages.push(<br></br>);
    this.setState({messages: newMessages});
  }

  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row>
            <Grid.Column width={6}>
              <Segment inverted className="panel-black">
                <Header size="medium">Messages</Header>
                <List selection verticalAlign='middle'>
                  <List.Item>
                    <Image avatar src='https://react.semantic-ui.com/images/avatar/small/helen.jpg' />
                    <List.Content>
                      <List.Header>Helen James</List.Header>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <Image avatar src='https://react.semantic-ui.com/images/avatar/small/christian.jpg' />
                    <List.Content>
                      <List.Header>Christian Phillips</List.Header>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <Image avatar src='https://react.semantic-ui.com/images/avatar/small/daniel.jpg' />
                    <List.Content>
                      <List.Header>Daniel Craig</List.Header>
                    </List.Content>
                  </List.Item>
                </List>
              </Segment>
            </Grid.Column>
            <Grid.Column width={10}>
              <Segment inverted className="panel-black">
                <Header size="medium">Christian Phillips</Header>
                <div className="row">
                  <div className="col-md-12">
                    <div className="thumbnail">
                      <div className="caption">
                        {this.state.messages}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-9">
                    <textarea className="form-control" name="newMessage" onChange={this.handleChange}></textarea>
                  </div>
                  <div className="col-md-2">
                    <button className="btn btn-uth-submit" onClick={this.send}>Submit</button>
                  </div>
                </div>
              </Segment>
            </Grid.Column>
          </Grid.Row> 
        </Grid>
      </Container>
    );
  }
}

export default Messaging;
