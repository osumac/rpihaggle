import React, { Component } from 'react';
import { Icon, Container, Header, Grid, Segment, Image } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import UserAccountLinks from '../components/UserAccountLinks';
import FavoritesList from '../components/FavoritesList';
import LookingForList from '../components/LookingForList';
import UserDetailsList from '../components/UserDetailsList';
import UpdateUserDetailsForm from '../components/UpdateUserDetailsForm';
import {isLoggedIn} from '../util/util'
import {withRouter} from 'react-router-dom';

// Purpose: A full page with multiple child components that contain all of the information
//          including their favorites, looking for, and actions for the current user
// State: 
//      user: contains information about the user
//      showForm: boolean to determine if the edit form should be showed
//      userID: current user's id
// Definition:
class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      userID: localStorage.getItem('id'),
      showForm: false
    }
    this.showUpdateForm = this.showUpdateForm.bind(this);
  }

  // Purpose: save a favorite to the user's page
  // Definition: 
  saveFavorite(event) {
    //event.preventDefault();
    console.log(this.state.userID);
    
  }

  // Purpose: toggle the update information form being shown 
  // Definition: 
  showUpdateForm(){
    this.setState({ showForm: !this.state.showForm });
  }

  // Purpose: load the user's data when the compponent loads
  // Definition: 
  componentDidMount() {
    
    /* if(!isLoggedIn()){
      this.props.history.push({
        pathname: '/Login'});
    } */
    
    let self = this;
    fetch('/user/' + this.state.userID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ user: data[0] });
    }).catch(err => {
      console.log('Error getting user data:');
      throw(err);
    })
  }
  // Purpose: render the profile component to the page
  // Definition: 
  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row>
            <Grid.Column width={6}>
              <Segment inverted className="panel-black">
                <Header size="medium">My Account</Header>
                <Grid>
                  <UserAccountLinks/>
                </Grid>
              </Segment>
              <Segment inverted className="panel-black">
                <Header size="medium">Favorites</Header>
                <Grid>
                  <FavoritesList id={this.state.userID}/>
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column width={10}>
              <Segment inverted className="panel-black">
                <Header size="medium">My Profile&nbsp;&nbsp;
                  <Icon name='pencil' link size='small' onClick={this.showUpdateForm} ></Icon>
                </Header>
                <Grid>
                  <Grid.Column floated='right' width={8}>
                    <div hidden={this.state.showForm}>
                      <UserDetailsList id={this.state.userID} />
                    </div>
                    <div hidden={!this.state.showForm}>
                      <UpdateUserDetailsForm id={this.state.userID}/>
                    </div>
                  </Grid.Column>
                  <Grid.Column floated='right' width={5}>
                    <Image src={"https://s3.amazonaws.com/rpihaggle-images/" + (this.state.user.filename ? this.state.user.filename : 'avatars/profile-icon.png') } />
                  </Grid.Column>
                </Grid>  
                <br/>         
                <Header>Looking For&nbsp;&nbsp;
                  <Icon name='pencil' link size='small'></Icon>
                </Header>
                <Grid>
                  <Grid.Column floated='right' width={15}>
                    <LookingForList id={this.state.userID}/>
                  </Grid.Column>
                </Grid>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default  withRouter(Profile);
