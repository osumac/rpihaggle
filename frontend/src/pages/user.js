import React, { Component } from 'react';
import { Icon, Container, Header, Grid, Segment, Image } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import UserAccountLinks from '../components/UserAccountLinks';
import FavoritesList from '../components/FavoritesList';
import LookingForList from '../components/LookingForList';
import UserDetailsList from '../components/UserDetailsList';
import {isLoggedIn} from '../util/util'
import {withRouter} from 'react-router-dom';

// Purpose: A full page with multiple child components that contain all of the information
//          including their favorites, looking for, and actions for another site user
// State: 
//      user: contains information about the user
//      viewUserID: the id of the user the page is loading
//      userID: current user's id
// Definition:
class User extends Component {
  constructor(props) {
    super(props);
    const splitUrl = this.props.location.pathname.split('/');
    this.state = {
      user: [],
      viewUserID: splitUrl[splitUrl.length - 1],
      userID: localStorage.getItem('id')
    }
  }

  // Purpose: when this component finishes loading, the application calls an api to get the users information
  // Definition:
  componentDidMount() {
    
      /* if(!isLoggedIn()){
        this.props.history.push({
          pathname: '/Login'});
      } */
    
    let self = this;
    console.log(this.state.viewUserID);
    fetch('/user/'+this.state.viewUserID, {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ user: data[0] });
    }).catch(err => {
      console.log('Error getting user data:');
      throw(err);
    })
  }
  // Purpose: loads the contents of the User page to the screen
  // Definition:
  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row>
            <Grid.Column width={6}>
              <Segment inverted className="panel-black">
                <Header size="medium">My Account</Header>
                <Grid>
                  <UserAccountLinks/>
                </Grid>
              </Segment>
              <Segment inverted className="panel-black">
                <Header size="medium">Favorites&nbsp;&nbsp;
                  <Icon name='pencil' link size='small'></Icon>
                </Header>
                <Grid>
                  <FavoritesList id={this.state.userID}/>
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column width={10}>
              <Segment inverted className="panel-black">
                <Header size="medium" content={this.state.user.name + "'s Profile"}>
                </Header>
                <Grid>
                  <Grid.Column floated='right' width={8}>
                    <UserDetailsList id={this.state.viewUserID} />
                  </Grid.Column>
                  <Grid.Column floated='right' width={5}>
                    <Image src={"https://s3.amazonaws.com/rpihaggle-images/" + this.state.user.filename} />
                  </Grid.Column>
                </Grid>  
                <br/>         
                <Header>Looking For</Header>
                <Grid>
                  <Grid.Column floated='right' width={15}>
                    <LookingForList id={this.state.viewUserID} />
                  </Grid.Column>
                </Grid>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default  withRouter(User);
