import React, { Component } from 'react';
import { Container, Header, Grid, Segment } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import FavoritesList from '../components/FavoritesList';
import Listings from '../components/Listings';
import {isLoggedIn} from '../util/util'
import {withRouter} from 'react-router-dom';

// Purpose: show listings to users who want to trade on the market
// state:
//    userID: id of the user viewing the marketplace
// Definition: 
class Marketplace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userID: localStorage.getItem('id'),
      listings: localStorage.getItem('searchListings') || []
    };
  }
  componentDidMount(){
    /* if(!isLoggedIn()){
      this.props.history.push({
        pathname: '/Login'});
    } */
  }

  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row>
            <Grid.Column width={6}>
              <Segment inverted className="panel-black">
                <Header size="medium">Favorites</Header>
                <Grid>
                  <FavoritesList id={this.state.userID} />
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column width={10}>
              <Segment inverted className="panel-black">
                <h2>Listings</h2>
                <div className="row">
                  <Listings listings={this.state.listings} />
                </div>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default  withRouter(Marketplace);
