import React, { Component } from 'react';
import { Container, Grid, Image } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {isLoggedIn} from '../util/util'
import 'semantic-ui-css/semantic.min.css';
import {withRouter, Link} from 'react-router-dom';


// Purpose: display a landing page for a user when they log in 
// Definition: 
class Home extends Component {

  componentDidMount(){
    /* if(!isLoggedIn()){
      this.props.history.push({
        pathname: '/Login'});
    } */
  }

  render() {
    return (
      <Container className="app-main-menu">
        <Grid>
          <Grid.Row centered columns={3} width={15}>
            <Grid.Column width={5} style={{textAlign:"center"}}>
              <h2>My Stuff</h2>
              <Link className="menu-icon" to='/profile'>
                <Image src="/images/Settings-512.png" />
              </Link>
            </Grid.Column>
            <Grid.Column width={5} style={{textAlign:"center"}}>
              <h2>Market</h2>
              <Link className="menu-icon" to='/marketplace'>
                <Image src="/images/Marketplace-512.png" />
              </Link>
            </Grid.Column>
            <Grid.Column width={5} style={{textAlign:"center"}}>
              <h2>Messaging</h2>
              <Link className="menu-icon" to='/messaging'>
                <Image src="/images/Chatting-512.png" />
              </Link>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default  withRouter(Home);
