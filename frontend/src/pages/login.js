import React, { Component } from 'react';
import 'react-intl-tel-input/dist/main.css';
import 'react-select/dist/react-select.css';
import { Message, Grid, Container, Header, Form, Button} from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {withRouter, Link} from 'react-router-dom';
import {doLogin} from '../util/api';
import {isLoggedIn} from '../util/util'


// Purpose: Login a user ot the application
// State:
//      email: user's email address
//      password: user's password
//      error: if there is an error logging in
// Definition: 
export default withRouter(class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      error: false,
      errorMessage: ''
    }
    this.logChange = this.logChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  // Purpose: when a user hits submit, this function will try to log them in
  // example: user submits thier information, the login function is called and if it succeeds, the jwt
  //          expires time, and id are stored in the localstorage for the application to use. the user
  //          is then sent to the ladning page
  // Definition: 
  handleSubmit(event) {
    event.preventDefault();
    var data = {
      email: this.state.email,
      password: this.state.password
    }
    doLogin(data).then((response, err) =>{
      if (err)
        throw err;
      if(!response){
        this.setState({error: true, errorMessage: "Server Error"});
      }
      else if (response.loggedIn){
        localStorage.setItem('jwt', response.jwt);
        localStorage.setItem('expires', response.expires);
        localStorage.setItem('id', response.id);
        localStorage.setItem('user', response.user);

        this.props.history.push({
          pathname: '/Home'});
      } else {
        this.setState({error: true, errorMessage: "Invalid username and password."});
      }
    });
  }

  // Purpose: when login is loaded, check to see if the user is already logged in. if they are send them to the landing page
  // Definition: 
  componentDidMount(){
    if(isLoggedIn()){
      this.props.history.push({
        pathname: '/Home'});
    }
  }

  // Purpose: update the state when a user changes form fields
  // Definition: 
  logChange(e) {
    if (e.target) {
      if (e.target.name) {
        this.setState({ [e.target.name]: e.target.value });
      }
    }
  }

  render() {
    return (
      <Container className="register-form">
        <Grid centered columns={2}>
          <Grid.Row centered columns={2} className="heading-section">
            <Header size='huge' className="main-heading">User Login</Header>
          </Grid.Row>
          <Grid.Row centered columns={2}>
            <Message
              header={this.state.errorMessage}
              color='black'
              compact
              hidden={!this.state.error}
            />
          </Grid.Row>
          <Grid.Row centered columns={2}>
            <Grid.Column width={14}>
              <Form onSubmit={this.handleSubmit} method="POST">
                <Grid.Row className="panel panel-default p50 uth-panel">
                  <Grid.Row className="panel-body uth-panel-body">
                    <Grid.Row className="col-md-12">
                      <div className="form-wrap">
                        <label>Email</label>
                        <input onChange={this.logChange} className="form-control"  placeholder='email@rpi.edu' name='email' validations={['required', 'email']} />
                      </div>
                    </Grid.Row>
                    <Grid.Row className="col-md-12">
                      <div className="form-wrap">
                        <label>Password</label>
                        <input onChange={this.logChange} className="form-control" type='password'  name='password' validations={['required', 'password']} />
                      </div>
                    </Grid.Row>
                    <Grid.Row className="submit-section">
                      <Button type='submit' size='massive' color='grey'>Submit</Button>
                    </Grid.Row>
                    <Grid.Row centered width={2} style={{textAlign:'center'}}>
                      <Link to="/register">Don't have an account? Signup!</Link>
                    </Grid.Row>
                  </Grid.Row>
                </Grid.Row>
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
})
