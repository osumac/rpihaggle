import React, { Component } from 'react';
import { Container, Header, Grid, Segment } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import FavoritesList from '../components/FavoritesList';
import Listing from '../components/Listing';
import LookingForList from '../components/LookingForList';
import {isLoggedIn} from '../util/util'
import {withRouter} from 'react-router-dom';

// Purpose: show information about a single listing on the application
// state: 
//      listingID: id of the listing being viewed
//      userID: id of the logged in user
// Definition: 
class ListingView extends Component {
  constructor(props) {
    super(props);
    const splitUrl = this.props.location.pathname.split('/');
    this.state = { 
      listingID: splitUrl[splitUrl.length - 1],
      userID: localStorage.getItem('id')
    };
  }
  componentDidMount(){
    /* if(!isLoggedIn()){
      this.props.history.push({
        pathname: '/Login'});
    } */
  }
  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row>
            <Grid.Column width={6}>
              <Segment inverted className="panel-black">
                <Header size="medium">Favorites</Header>
                <Grid>
                  <FavoritesList id={this.state.userID}/>
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column width={10}>
              <Segment inverted className="panel-black">
                
                <Listing listingID={this.state.listingID} userID={this.state.userID}/>
                
                <hr/>
                <Grid.Column>
                  <Header>This Trader is Looking For:  </Header>
                  <Grid.Column floated='right' width={15}>
                    <LookingForList listingID={this.state.listingID}/>
                  </Grid.Column>
                </Grid.Column>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}


export default  withRouter(ListingView);
