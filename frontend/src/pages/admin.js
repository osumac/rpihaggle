import React, { Component } from 'react';
import { Container, Table, Segment, Header } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {Link} from 'react-router-dom';

// Purpose: display a list of users for admin's to take actions on
// Definition: 
export default class Users extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      reports: []
    }
  }
  // Purpose: load all users in the application and put them in the state
  // Definition: 
  componentDidMount() {
    let self = this;
    fetch('/users', {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ users: data });
    }).catch(err => {
      console.log('error fetching users: ', err);
    })

    fetch('/reports', {
      method: 'GET'
    }).then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    }).then(function (data) {
      self.setState({ reports: data });
    }).catch(err => {
      console.log('error fetching reports: ', err);
    })
  }

  render() {
    return (
      <Container>
        <Segment inverted className="panel-black">
          <Header>Users</Header>
          <Table inverted className="black table table-hover">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Email</Table.HeaderCell>
                <Table.HeaderCell>Phone Number</Table.HeaderCell>
                <Table.HeaderCell>Address</Table.HeaderCell>
                <Table.HeaderCell>Role</Table.HeaderCell>
                <Table.HeaderCell>Date Created</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {this.state.users.map(member =>
                <Table.Row key={member.id}>
                  <Table.Cell><Link to={'/user/'+ member.id}>{member.name}</Link></Table.Cell>
                  <Table.Cell>{member.email}</Table.Cell>
                  <Table.Cell>{member.phoneNumber}</Table.Cell>
                  <Table.Cell>{member.address}</Table.Cell>
                  <Table.Cell>{member.role}</Table.Cell>
                  <Table.Cell>{member.dateCreated}</Table.Cell>
                  <Table.Cell><Link to='#'>Edit</Link> | <Link to='#'>Delete</Link></Table.Cell>
                </Table.Row>
              )}
            </Table.Body>
          </Table>

          <Header>Reports</Header>
          <Table inverted className="black table table-hover">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>UserID</Table.HeaderCell>
                <Table.HeaderCell>Reason</Table.HeaderCell>
                <Table.HeaderCell>Description</Table.HeaderCell>
                <Table.HeaderCell>ListingID</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {this.state.reports.map(report =>
                <Table.Row key={report.id}>
                  <Table.Cell><Link to={'/user/'+ report.reporterID}>{report.reporterID}</Link></Table.Cell>
                  <Table.Cell>{report.reason}</Table.Cell>
                  <Table.Cell>{report.description}</Table.Cell>
                  <Table.Cell><Link to={'/listing/'+ report.listingID}>{report.listingID}</Link></Table.Cell>
                  <Table.Cell><Link to='#'>Edit</Link> | <Link to='#'>Delete</Link></Table.Cell>
                </Table.Row>
              )}
            </Table.Body>
          </Table>
        </Segment>
      </Container>
    );
  }
}