// src/routes.js
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './pages/home.js';
import NotFound from './components/notfound';
import Register from './pages/register.js';
import Login from './pages/login.js';
import Admin from './pages/admin.js';
import Marketplace from './pages/marketplace.js';
import MyListings from './pages/myListings.js';
import Messaging from './pages/messaging.js';
import Profile from './pages/profile.js';
import User from './pages/user.js';
import ListingView from './pages/listing.js';
import NewListing from './pages/newListing.js';


// Purpose: Create a frontend router which loads a component based on the path in the address bar
// Example: url ends in /Home so the router will load the landing page component
// Definition:
const Routes = () => (
	<Switch>
		<Route exact path="/" component={Login} />
		<Route path="/Register" component={Register} />
		<Route path="/Home" component={Home} />
		<Route path="/Admin" component={Admin} />
		<Route path="/Login" component={Login} />
		<Route path="/Marketplace" component={Marketplace} />
		<Route path="/MyListings" component={MyListings} />
		<Route exact path="/Listing/:id" component={ListingView} />
		<Route exact path="/NewListing" component={NewListing} />
		<Route path="/Messaging" component={Messaging} />
		<Route path="/Profile" component={Profile} />
		<Route path="/User" component={User} />
		<Route path="*" component={NotFound} />
	</Switch>
);

export default Routes;