var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var session = require('express-session');
var ssn;
var cookieSession = require('cookie-session');
var index = require('./routes/index');
var cors = require('cors');
var db = require('./module/queries');

var app = express();
/* app.use(session({
  secret:'s3Cur3',
  resave: true,
  saveUninitialized: true
})); */


app.set('trust proxy', 1); // trust first proxy
app.use(cors());
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2'],
  // Cookie Options
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'frontend/build')));
app.use(cookieParser('rpihaggle'));

app.use('/', index);
//app.use('/users', users);
app.get('/',function(req,res){
  console.log('req', req);
  ssn=req.session;
  console.log('ssn', ssn);

});
// database querires
app.get('/users', db.getUsers);
app.get('/user/:id', db.getUserByID);
app.get('/reports', db.getReports);
app.get('/favorites/:id', db.getFavoritesByUserID);
app.get('/listing/:id', db.getListingByID);
app.get('/myListings/:id', db.getListingsByUserID);
app.get('/allListings', db.getListings);
app.get('/listingImages/:id', db.getListingImages);
app.get('/lookingFor/:id', db.getLookingFor);
app.get('/lookingForByListingID/:id', db.getLookingForByListingID);
app.get('/tags', db.getTags);
app.post('/addFavorite', db.addFavorite);
app.post('/removeFavorite', db.removeFavorite);
app.post('/updateUser', db.updateUser);
app.post('/updateListing', db.updateListing);
app.post('/reportListing', db.reportListing);
app.post('/register', db.createUser);
app.post('/login', db.logIn);
app.post('/createNewListing', db.createNewListing);
app.post('/searchListings', db.searchListings);
//app.put('/users/:email', db.updateUser);
app.delete('/users/:email', db.deleteUser);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log('[ERR] ' + err)
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

const PORT = process.env.PORT || 5000;
var server = http.createServer(app);
server.listen(PORT, () => {
  console.log(`Express is running on port ${PORT}`);
}); 
