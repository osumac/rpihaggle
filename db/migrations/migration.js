exports.up = function(knex, Promise) {
  return Promise.all([
		knex.schema.createTable('userRoles', (table) => {
  		table.increments('id').primary();
  		table.string('role');
		}),
		knex.schema.createTable('avatars', (table) => {
  		table.increments('id').primary();
			table.string('filename');
  	}),
  	knex.schema.createTable('users', (table) => {
  		table.increments('id').primary();
  		table.string('name');
  		table.string('email').notNullable();
			table.string('password');
			table.string('screenName');
  		table.string('phoneNumber');
  		table.string('address');
  		table.integer('roleID');
  		table.integer('avatarID');
  		table.string('dateCreated');
			table.string('dateUpdated');
			
  		table.foreign('roleID').references('id').inTable('userRoles');
  		table.foreign('avatarID').references('id').inTable('avatars');
		}),
		knex.schema.createTable('tags', (table) => {
  		table.increments('id').primary();
			table.string('name');
			table.integer('parentTagID').nullable();

			table.foreign('parentTagID').references('id').inTable('tags');
		}),
		knex.schema.createTable('lookingFor', (table) => {
  		table.increments('id').primary();
			table.integer('userID');
			table.string('description');
			table.integer('tagID');
			table.string('dateCreated');

			table.foreign('userID').references('id').inTable('users');
			table.foreign('tagID').references('id').inTable('tags');
  	}),
		knex.schema.createTable('listingStatuses', (table) => {
  		table.increments('id').primary();
			table.string('status');
		}),
		knex.schema.createTable('listingConditions', (table) => {
  		table.increments('id').primary();
			table.string('condition');
		}),
		knex.schema.createTable('listings', (table) => {
  		table.increments('id').primary();
  		table.string('name');
  		table.integer('userID');
			table.string('description');
  		table.string('dateCreated');
  		table.integer('statusID');
  		table.integer('conditionID');
  		table.boolean('isFree');
			table.string('dateUpdated');
			
			table.foreign('userID').references('id').inTable('users');
  		table.foreign('statusID').references('id').inTable('listingStatuses');
  		table.foreign('conditionID').references('id').inTable('listingConditions');
		}),
		knex.schema.createTable('listingImages', (table) => {
  		table.integer('listingID');
			table.string('filename');

  		table.foreign('listingID').references('id').inTable('listings');
		}),
		knex.schema.createTable('listingTags', (table) => {
  		table.integer('listingID');
			table.integer('tagID');

  		table.foreign('listingID').references('id').inTable('listings');
			table.foreign('tagID').references('id').inTable('tags');
		}),
		knex.schema.createTable('favorites', (table) => {
  		table.integer('userID');
			table.integer('listingID');

  		table.foreign('userID').references('id').inTable('users');
			table.foreign('listingID').references('id').inTable('listings');
  	}),
		knex.schema.createTable('tradeStatuses', (table) => {
  		table.increments('id').primary();
			table.string('status');
		}),
		knex.schema.createTable('trades', (table) => {
  		table.increments('id').primary();
			table.integer('listingID');
			table.integer('proposerID');
			table.integer('tradeStateID');
			table.string('dateCreated');
			table.string('dateUpdated');

			table.foreign('listingID').references('id').inTable('listings');
			table.foreign('proposerID').references('id').inTable('users');
			table.foreign('tradeStateID').references('id').inTable('tradeStatuses');
		}),
		knex.schema.createTable('reportStatuses', (table) => {
  		table.increments('id').primary();
			table.string('status');
		}),
		knex.schema.createTable('reports', (table) => {
  		table.increments('id').primary();
			table.integer('reporterID');
			table.string('reason');
			table.string('description');
			table.string('response');
			table.integer('adminResponder');
			table.integer('statusID');
			table.integer('listingID').nullable();
			table.string('dateCreated');
			table.string('dateUpdated');

			table.foreign('reporterID').references('id').inTable('users');
			table.foreign('adminResponder').references('id').inTable('users');
			table.foreign('statusID').references('id').inTable('reportStatuses');
			table.foreign('listingID').references('id').inTable('listings');
		})
	]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
		knex.schema.dropTableIfExists('favorites'),
    knex.schema.dropTableIfExists('lookingFor'),
    knex.schema.dropTableIfExists('listingImages'),
    knex.schema.dropTableIfExists('listingTags'),
    knex.schema.dropTableIfExists('trades'),
    knex.schema.dropTableIfExists('tags'),
    knex.schema.dropTableIfExists('reports'),
    knex.schema.dropTableIfExists('reportStatuses'),
    knex.schema.dropTableIfExists('tradeStatuses'),
    knex.schema.dropTableIfExists('listings'),
    knex.schema.dropTableIfExists('listingStatuses'),
    knex.schema.dropTableIfExists('listingConditions'),
    knex.schema.dropTableIfExists('users'),
    knex.schema.dropTableIfExists('userRoles'),
    knex.schema.dropTableIfExists('avatars')
  ]);
};