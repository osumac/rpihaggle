const childTagsData = require('../../data/childTags');
const listingsData = require('../../data/listings');
const lookingForData = require('../../data/lookingFor');
const usersData = require('../../data/users');


exports.seed = function(knex, Promise) {
  return knex('lookingFor').del()
    .then(() => {
      return knex('listings').del();
    })
    .then(() => {
      return knex('users').del();
    })
    .then(() => {
      return knex('tags').insert(childTagsData);
    })
    .then(() => {
      return knex('users').insert(usersData);
    })
    .then(() => {
      return knex('listings').insert(listingsData);
    })
    .then(() => {
      return knex('lookingFor').insert(lookingForData);
    })
};