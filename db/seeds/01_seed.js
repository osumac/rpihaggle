const listingConditionsData = require('../../data/listingConditions');
const listingStatusesData = require('../../data/listingStatuses');
const reportStatusesData = require('../../data/reportStatuses');
const tagsData = require('../../data/tags');
const tradeStatusesData = require('../../data/tradeStatuses');
const userRolesData = require('../../data/userRoles');
const avatarsData = require('../../data/avatars');


exports.seed = function(knex, Promise) {
  return knex('users').del()
    .then(() => {
      return knex('userRoles').del();
    })
    .then(() => {
      return knex('listingStatuses').del();
    })
    .then(() => {
      return knex('listingConditions').del();
    })
    .then(() => {
      return knex('reportStatuses').del();
    })
    .then(() => {
      return knex('tags').del();
    })
    .then(() => {
      return knex('tradeStatuses').del();
    })
    .then(() => {
      return knex('avatars').del();
    })
    .then(() => {
      return knex('listingStatuses').insert(listingStatusesData);
    })
    .then(() => {
      return knex('listingConditions').insert(listingConditionsData);
    })
    .then(() => {
      return knex('reportStatuses').insert(reportStatusesData);
    })
    .then(() => {
      return knex('tags').insert(tagsData);
    })
    .then(() => {
      return knex('tradeStatuses').insert(tradeStatusesData);
    })
    .then(() => {
      return knex('userRoles').insert(userRolesData);
    })
    .then(() => {
      return knex('avatars').insert(avatarsData);
    })
    
};