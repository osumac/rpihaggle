const favoritesData = require('../../data/favorites');
const listingImagesData = require('../../data/listingImages');


exports.seed = function(knex, Promise) {
  return knex('favorites').del()
    .then(() => {
      return knex('favorites').insert(favoritesData);
    })
    .then(() => {
      return knex('listingImages').insert(listingImagesData);
    })
};